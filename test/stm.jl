using DifferentialEquations
using ForwardDiff
using FiniteDifferences
using LinearAlgebra
using BenchmarkTools
include("../src/dynamics/cr3bp_dyn.jl")

function propcr3bp(X₀, μ)
    # Propagate CR3BP EOMs
    cr3bpprob = ODEProblem(cr3bp!,X₀,[0.,10.],μ)
    sol = solve(cr3bpprob, Vern7(), reltol=1e-10)
    Xₜ = sol[end]
end

function comparestms()
    # X₀ = zeros(Float64, 6)
    X₀ = rand(Float64, 6)
    println("X0: $(X₀).")
    X₀S = [X₀; vec(Matrix{Float64}(I,6,6))]
    μ = 0.3

    # Analytical (propagated) STM
    cr3bpstmprob = ODEProblem(cr3bpstm!,X₀S,[0.,10.],μ)
    analytical_time = @elapsed sol = solve(cr3bpstmprob, Vern7(), reltol=1e-10)
    stmprop = reshape(sol[end][7:end],(6,6))

    # Autodiff STM
    autodiff_time = @elapsed stmad = ForwardDiff.jacobian(X->propcr3bp(X,μ),X₀)

    # Finite differenced STM
    findiff_time = @elapsed stmfd = jacobian(central_fdm(7,1), X->propcr3bp(X,μ), X₀)[1]

    println("Propagated STM runtime: $analytical_time")

    println("Autodiff'd STM error: $(stmad-stmprop)")
    println("Autodiff'd STM runtime: $autodiff_time")
    println("Finite differenced STM error: $(stmfd-stmprop)")
    println("Finite differenced STM runtime: $findiff_time")
    # @benchmark ForwardDiff.jacobian(X->propcr3bp(X,μ),X₀) setup=(X₀=rand(Float64,6), μ=0.3)
    # @benchmark solve(cr3bpstmprob, Vern7(), reltol=1e-10)
    # @benchmark jacobian(central_fdm(7,1), X->propcr3bp(X,μ), X₀) setup=(X₀=rand(Float64,6), μ=0.3)

end

comparestms()