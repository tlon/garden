# Garden

The garden of forking paths. All lose their way here, in both time and space. The phase space forks endlessly, beware, mortals.